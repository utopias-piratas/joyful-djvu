# Objetivo

Genera un archivo DjVu con capa de OCR a partir de un PDF escaneado.
Realiza el proceso de extracción de páginas, rotado si es necesario,
aunque no puede limpiarlas automáticamente con ScanTailor porque no
tiene las features necesarias.

Luego de la limpieza manual/visual, procesa las páginas limpias para
extraer el OCR y luego combinarlo en un archivo DjVu.

# Uso

Cada paso del `Makefile` está documentado, para ver la ayuda básica,
ejecutar:

```bash
make
# o bien
make help
```

El `Makefile` tiene una regla por cada paso.  Las reglas se pueden
combinar para hacer todos los pasos uno detrás de otro:

```bash
# rotate-180 es opcional en caso que las páginas hayan venido escaneadas
# al revés
make extract-images rotate-180
# (Procesar manualmente con ScanTailor Advanced)
# (Varias horas después...)
make ocr djvu djvu-with-ocr book file=libro.pdf
```

Algunas reglas necesitan que se pase como argumento el nombre del
archivo PDF que se va a procesar, esto se hace con la variable `file=`:

```bash
make file=nombre_del_archivo.pdf extract-images
make file=nombre_del_archivo.pdf book
```

También se puede editar el `Makefile` definiendo el nombre del archivo
en la variable `file` en lugar de explicitarlo como argumento cada vez.

Otras opciones:

* `lang=` indica el idioma de cuneiform a utilizar, por ejemplo `spa`
  para el castellano.

```bash
make lang=spa ocr
```

# Recomendación

Cambiar el nombre de archivo a algo que no tenga espacio ni caracteres
especiales, solo letras y números, sino el sistema se confunde.

# Dependencias

Paquetes a instalar:

* cuneiform (y sus paquetes de idioma)
* ocrodjvu
* djvulibre
* imagemagick
* scantailor advanced (no el común, porque las últimas versiones
  confunden a cuneiform, ver el `Makefile`)
* poppler
