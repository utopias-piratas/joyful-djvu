# Asegurarse que la shell que estamos usando sea bash, porque algunas
# distribuciones usan shells más limitadas (por ejemplo Debian y
# derivadas)
SHELL := /bin/bash
# La ayuda se muestra por defecto
.DEFAULT_GOAL := help
# El archivo PDF con el que vamos a trabajar, el nombre no tiene que
# contener espacios, idealmente sólo letras y números y la extensión
# .pdf
file ?= libro.pdf
# El código de idioma para mejorar el OCR, según `cuneiform`:
# eng ger fra rus swe spa ita ruseng ukr srp hrv pol dan por dut cze rum
# hun bul slv lav lit est tur
lang ?= eng
# El prefijo con el que vamos a trabajar, por defecto el nombre del
# directorio actual, que tampoco deberia contener espacios.
prefix ?= $(shell pwd | xargs basename)

# Las páginas originales, esta variable se completa una vez que se
# corrió la regla `extract-images`, por eso usamos `=` en lugar de `:=`,
# es decir actualizar la variable cada vez que se la llama en lugar de
# actualizarla una sola vez (o configurarla en diferido en lugar de
# inmediatamente).
original_pages = $(shell ls -1 original_pages/*.tif)
# La lista de páginas rotadas, solo cambia el directorio
rotated_pages = $(patsubst original_pages/%,rotated_pages/%,$(original_pages))
# Páginas ya limpiadas por ScanTailor
cleaned_pages = $(patsubst original_pages/%.tif,cleaned_pages/%.html,$(original_pages))
# Páginas con la resolución arreglada
fixed_pages = $(patsubst original_pages/%,fixed_pages/%,$(original_pages))
# Páginas con OCR
ocr_pages = $(patsubst original_pages/%.tif,ocr_pages/%.html,$(original_pages))
# Páginas en DjVu
djvu_pages = $(patsubst original_pages/%.tif,djvu_pages/%.djvu,$(original_pages))
# Páginas en DjVu con capa de OCR
djvu_ocr_pages = $(patsubst original_pages/%.tif,djvu_ocr_pages/%.djvu,$(original_pages))
# Páginas de texto plano
txt_pages = $(patsubst original_pages/%.tif,txt_pages/%.txt,$(original_pages))


# Los directorios a crear, cada vez que se llame a uno de estos en una
# regla, se corre esta que los crea si no existen.
dirs := original_pages rotated_pages cleaned_pages ocr_pages djvu_pages djvu_ocr_pages txt_pages
$(dirs):
	mkdir -p $@

# Extrae todas las páginas como imágenes TIFF, tal vez listas para pasar
# por ScanTailor.  Al finalizar, crear un archivo del mismo nombre para
# no repetir la regla.  Este archivo se puede borrar a mano o
# automáticamente usando `clean`.
extract-images: $(file) original_pages ## Extraer todas las páginas como imágenes TIFF.  Necesita file=archivo.pdf
	pdfimages -tiff "$<" $(prefix)

# Regla para rotar cada página individualmente
rotated_pages/%.tif: original_pages/%.tif
	convert -rotate 180 -verbose $< $@

# Rotar las páginas 180 grados, si el escaneo vino invertido.
rotate-180: rotated_pages $(rotated_pages) ## Rotar las páginas 180 grados

# Decide si las páginas que vamos a usar en ST son las rotadas o las
# originales, dependiendo si usamos la regla que rota las páginas antes.
pages = $(shell if test -d rotated_pages; then echo rotated_pages ; else echo original_pages ; fi)
# Corre el ScanTailor automáticamente, tal vez quieras comenzarlo a mano
# para probar.
#
# Hay que decirle que haga los 6 pasos o los archivos no se guardan!
# https://bbs.archlinux.org/viewtopic.php?id=207478
#
# XXX Finalmente no conviene hacer este paso automáticamente porque
# ScanTailor CLI no tiene forma de controlar la escala de salida,
# siempre la convierte a x2.  Además ni ST CLI ni GUI 0.9.12.2 almacenan
# la resolución del archivo TIFF, con lo que cuneiform luego se confunde
# y no sabe a qué resolución leer.
#
# El proceso manual es abrir en scantailor-advanced un nuevo proyecto
# donde el directorio de entrada sea `original_pages` o `rotated_pages`
# según se hayan rotado y que el de salida sea `cleaned_pages` (en lugar
# de `out` dentro de `original_pages`).  Realizar la limpieza
# manualmente y al terminar volver a este Makefile.
scantailor: cleaned_pages ## Corre ScanTailor automáticamente (no recomendado)
	scantailor-cli --verbose --white-margins \
		--input-dpi=300 --output-dpi=300 \
		--start-filter=1 --end-filter=6 \
		--output-project=$(prefix).ScanTailor \
		$(pages) cleaned_pages/

# Todos los TIFF dentro de cleaned_pages/ se convierten en OCR.  Si la
# conversión falla, se saltea el error creando un archivo vacío (si todo
# va bien, cuneiform solo falla en páginas vacías de todas formas).
ocr_pages/%.html: cleaned_pages/%.tif
	cuneiform -l $(lang) -f hocr -o $@ $< || touch $@

# Aplica el OCR a todas las páginas del libro y las guarda como archivos
# hOCR.  Este formato almacena las coordenadas de cada oración, con lo
# que luego se pueden crear PDFs y DjVus seleccionables.
ocr: ocr_pages $(ocr_pages) ## OCR a todas las páginas

# Todos los TIFF limpios también se convierten a DjVu
djvu_pages/%.djvu: cleaned_pages/%.tif
	cjb2 -dpi 300 -lossless "$<" "$@"

# Crea todas las páginas DjVu convirtiéndolas desde los TIFF limpios
djvu: djvu_pages $(djvu_pages) ## Convierte los TIFF a DjVu

# Crea una página DjVu con OCR tomando el OCR y combinándolo con el
# DjVu.  Primero crea una copia del DjVu y luego chequea si el OCR fue
# realizado (puede ser un archivo vacío).  Entonces aplica la capa de
# OCR a la copia del Djvu
djvu_ocr_pages/%.djvu: ocr_pages/%.html djvu_pages/%.djvu
	cp $(lastword $^) $@
	if test -s "$<" ; then hocr2djvused < "$<" | djvused -s "$@"; fi

djvu-with-ocr: djvu_ocr_pages $(djvu_ocr_pages) ## Aplica la capa de OCR a los DjVu.  Necesita file=archivo.pdf

file_djvu := $(patsubst %.pdf,%.djvu,$(file))
$(file_djvu): $(djvu_ocr_pages)
	djvm -create "$@" $^

book: $(file_djvu) ## Genera un libro DjVu

txt_pages/%.txt: ocr_pages/%.html
	sed -re "s/<[^>]+>//g" "$<" > "$@"

txt: txt_pages $(txt_pages) ## Generar el texto plano

file_txt := $(patsubst %.pdf,%.txt,$(file))
$(file_txt): $(txt_pages)
	cat $^ > $@

txt-book: $(file_txt) ## Genera un solo archivo con el texto plano.  Necesita file=archivo.pdf

clean: ## Limpieza total, volver a empezar
	rm -rf $(dirs)

help: ## Muestra la ayuda
	@echo "OCR"
	@echo
	@echo "Uso: make [regla] [file=$(file)] [lang=$(lang)]"
	@echo
	@grep -E "^.+:.*##.*" $(MAKEFILE_LIST) \
	| grep -v grep \
	| grep -v sed \
	| sed -re "s/^(.+):.*##(.*)/\1;\2/" \
	| column -ts ';'
